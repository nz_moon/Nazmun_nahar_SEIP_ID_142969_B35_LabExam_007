<?php

namespace App\SummaryOfOrganization;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class SummaryOfOrganization extends DB{
    public $id;
    public $organization_name;
    public $organization_summary;

    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        echo  "I am in Summary Of Organization Class";
    }

    public function setData($postVariableData = NULL)
    {

        if (array_key_exists('id', $postVariableData)) {
            $this->id = $postVariableData['id'];
        }

        if (array_key_exists('organization_name', $postVariableData)) {
            $this->organization_name = $postVariableData['organization_name'];
        }

        if (array_key_exists('organization_summary', $postVariableData)) {
            $this->organization_summary = $postVariableData['organization_summary'];
        }
    }

    public function store()
    {
        $arrData=array($this->organization_name,$this->organization_summary);
        $sql = "Insert INTO summary_of_organiztion(organization_name,organization_summary) VALUES(?,?)";
        $STH = $this->DBH->prepare($sql);
        $result=$STH->execute( $arrData);

        if($result)
            Message::setMessage("Success! Data Has Been Insurted Successfully :) ");
        else
            Message::setMessage("Failed! Data Has Not Been Insurted Successfully :( ");

        Utility::redirect('create.php');


    }//end of store mathod
}