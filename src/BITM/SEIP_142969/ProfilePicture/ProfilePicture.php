<?php
namespace App\ProfilePicture;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class ProfilePicture extends DB
{
    public $id;
    public $name;
    public $image;
    public function __construct()
    {
        parent::__construct();
    }
    public function setData($postVariableData=Null)
    {
        if(array_key_exists('id',$postVariableData))
        {
            $this->id=$postVariableData['id'];
        }
        if(array_key_exists('name',$postVariableData))
        {
            $this->name=$postVariableData['name'];
        }
        if(array_key_exists('image',$postVariableData))
        {
            $this->image=$postVariableData['image'];
        }

    }
    public function store()
    {
        $arrData =array($this->name,$this->image);

        $sql="INSERT INTO profile_picture(name,image) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::setMessage("Success!Data has been inserted successfully");
        else
            Message::setMessage("Failed!Data has been inserted successfully");

        Utility::redirect('create.php');
    }
}