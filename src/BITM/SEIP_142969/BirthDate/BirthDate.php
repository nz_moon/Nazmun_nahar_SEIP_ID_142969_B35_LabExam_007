<?php

namespace App\BirthDate;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class BirthDate extends  DB{
    public $id;
    public $name;
    public $date;

    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        echo  "I am in Birthdate Class";
    }

        public function setData($postVariableData = NULL)
    {

        if (array_key_exists('id', $postVariableData)) {
            $this->id = $postVariableData['id'];
        }

        if (array_key_exists('name', $postVariableData)) {
            $this->name = $postVariableData['name'];
        }

        if (array_key_exists('date', $postVariableData)) {
            $this->date = $postVariableData['date'];
        }
    }

    public function store()
    {
        $arrData=array($this->name,$this->date);
        $sql = "Insert INTO birthdate(name,date) VALUES(?,?)";
        $STH = $this->DBH->prepare($sql);
        $result=$STH->execute( $arrData);

        if($result)
            Message::setMessage("Success! Data Has Been Insurted Successfully :) ");
        else
            Message::setMessage("Failed! Data Has Not Been Insurted Successfully :( ");

        Utility::redirect('create.php');


    }//end of store mathod

}